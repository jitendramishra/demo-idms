# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository consist of demo application of IDMS.


### How do I get set up? ###

## How to Download :
    git clone https://jitendramishra@bitbucket.org/jitendramishra/demo-idms.git
## Install Dependencies
    npm install<br/>
    bower install
## How to Run the application
     grunt serve (Now you can open browser with url http://localhost:9001)<br/>
     Note : Please dont change the port it is very specific to IDMS configuration if you change you could not able to work with IDMS.
