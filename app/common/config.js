'use strict';
angular.module('se-branding-config',[])
  .constant('seClient', (function() {
    return {
      CLIENT_ID: '3MVG9ahGHqp.k2_zHolxbafgOAJ5Mz3g_aolXs_w2lFqsBPsxdrvIJ92xEtjR60MO_7Vs_Y21w9zOmcqvg0RM',
      APP_ID:'eDlocal',
      APP_VERSION:'1.0',
      IDMS_AUTODISCOVERY_URL:'https://preprod19-secommunities.cs17.force.com/identity/services/apexrest/oidcsilent/autodiscovery',
      REDIRECT_URL:'http://localhost:9001',
      SILENT_REDIRECT_URL:'http://localhost:9001/silentRedirect.html',
      REGISTRATION_URL:'https://preprod19-secommunities.cs17.force.com/identity/userregistrationwork',
      EDIT_PROFILE_URL:'https://preprod19-secommunities.cs17.force.com/identity/UserProfile'
	  };
  })());
  // For local you set it modHeader as tnc-demo-local

