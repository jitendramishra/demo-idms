var NgOidcClient;
(function (NgOidcClient) {
    angular.module('ng.oidcclient', []);
    NgOidcClient.getModule = function () {
        return angular.module("ng.oidcclient");
    };
})(NgOidcClient || (NgOidcClient = {}));

(function (NgOidcClient) {
    "use strict";
    var app = NgOidcClient.getModule();
    var NgOidcClientProvider = (function () {
        function NgOidcClientProvider() {
            this.settings = null;
            this.mgr = null;
            this.userInfo = {
                user: null,
                isAuthenticated: false
            };
            this.urls = [];
            this.$get.$inject = ['$q', '$log', '$rootScope', 'seClient'];
        }
        NgOidcClientProvider.prototype.setSetting= function (options) {
            this.settings = options;
        };
        NgOidcClientProvider.prototype.setUrls = function (options) {
            this.urls = options;
        };
        NgOidcClientProvider.prototype.$get = function ($q, $log, $rootScope, seClient) {
            var _this = this;
            $log.log("NgOidcClient service started");
            if (!this.settings) {
                throw Error('NgOidcUserService: Must call setSettings() with the required options.');
            }
            /* jshint ignore:start */
            // Code here will be ignored by JSHint.

            Oidc.Log.logger = console;
            Oidc.Log.logLevel = Oidc.Log.INFO;
            this.mgr = new Oidc.UserManager(this.settings);
            /* jshint ignore:end */
            this.mgr.events.addUserLoaded(function (u) {
                _this.userInfo.user = u;
                _this.userInfo.isAuthenticated = true;
                notifyUserInfoChangedEvent();
            });
            this.mgr.events.addUserUnloaded(function () {
                _this.userInfo.user = null;
                _this.userInfo.isAuthenticated = false;
                
                $log.log("user unloaded");
                notifyUserInfoChangedEvent();
            });
            this.mgr.events.addAccessTokenExpiring(function () {
                $log.log("token expiring");
            });
            this.mgr.events.addAccessTokenExpired(function(){
                $rootScope.$broadcast("forceLogin");
            });
            this.mgr.events.addSilentRenewError(function (e) {
                _this.userInfo.user = null;
                _this.userInfo.isAuthenticated = false;
                $log.log("silent renew error", e.message);
                $rootScope.$broadcast("forceLogin");
                notifyUserInfoChangedEvent();
            });
            var signinRedirectCallback = function(url) {
                return _this.mgr.signinRedirectCallback(url).then(function(user) {
                    $log.log("signed in redirect callback", user);
                    return user;
                });
            };
            var signinRedirect = function (countryCode, language) {
                var extraParam= {
                    "app":seClient.APP_ID,
                    "lang":'fr-FR',
                    "countryCode":'FR'
                };
                return _this.mgr.signinRedirect({'extraQueryParams':extraParam})
                    .then(function (user) {
                    $log.log("signed in", user);
                    return user;
                })
                .catch(function (err) {
                    $log.log(err);
                    return err;
                });
            };
            var getUrls = function () {
                return _this.urls;
            };
            var getUserInfo = function () {
                return _this.userInfo;
            };
            var notifyUserInfoChangedEvent = function () {
                $rootScope.$emit('ng-oidcclient-userinfo-changed-event');
            };
            var userInfoChanged = function (scope, callback) {
                var handler = $rootScope.$on('ng-oidcclient-userinfo-changed-event', callback);
                scope.$on('$destroy', handler);
            };
            var getUserObjFromSession = function() {
                return _this.mgr.getUser();
            };
            function logOutRedirect() {
                return _this.mgr.signoutRedirect();
            }
            function getUserMgr() {
                return _this.mgr;
            }

            function getUserProfileByAccessToken() {  
                return _this.mgr.signinSilent().then(function(result){
                        return result;
                }).catch(function(error){
                    console.log(error);
                    return error;
                });
            }

            var createState = function() {
                return _this.mgr.createSigninRequest();
            };

            return {
                getUserInfo: getUserInfo,
                getUrls: getUrls,
                signRedirect: signinRedirect,
                signinRedirectCallback:signinRedirectCallback,
                userInfoChanged: userInfoChanged,
                getUserObjFromSession:getUserObjFromSession,
                logOutRedirect:logOutRedirect,
                getUserProfileByAccessToken:getUserProfileByAccessToken,
                getUserMgr:getUserMgr,
                createState:createState
            };
        };
        return NgOidcClientProvider;
    }());
    app.provider('ngOidcClient', NgOidcClientProvider);
})(NgOidcClient || (NgOidcClient = {}));