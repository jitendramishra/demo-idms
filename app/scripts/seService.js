angular.module("se-branding").
service("seService", function($window, seClient){
    this.getToken= function() {
        return $window.localStorage.getItem("access_token");
    }
    this.setToken = function(access_token) {
        return $window.localStorage.setItem("access_token", access_token);
    }
    this.setAccessTokenURL = function(tokenURL) {
        $window.localStorage.setItem('accessTokenURL', tokenURL);
    }
    this.getAccessTokenURL = function() {
        return $window.localStorage.getItem('accessTokenURL');
    }
    this.setLoginMode = function(loginMode) {
        return $window.localStorage.setItem("loginMode", loginMode);
    }
    this.getLoginMode = function() {
        return $window.localStorage.getItem("loginMode");
    }
    this.removeToken = function() {
        $window.localStorage.removeItem("access_token");
    }
    this.setIsEditProfile = function(isEditProfile) {
        $window.localStorage.setItem("isEditProfile", isEditProfile);
    }
    this.getIsEditProfile = function() {
        return $window.localStorage.getItem("isEditProfile")==='true';
    }
    this.register = function(lang, countryCode) {
        var registerURL = seClient.REGISTRATION_URL+"?app="+seClient.APP_ID+"&lang="+lang+"&countryCode="+countryCode;
        $window.open(registerURL, '_blank;')
    }
    this.updateProfile = function(lang, countryCode) {
        var editProfileURL = seClient.EDIT_PROFILE_URL+"?app="+seClient.APP_ID+"&lang="+lang+"&countryCode="+countryCode;
        //this.setLoginMode("login");
        this.setIsEditProfile(true);
        $window.open(editProfileURL, "_self");
    }
});