var loginModule = angular.module('loginModule',['se-branding-config']);
loginModule.controller('LoginController', LoginController);
LoginController.$inject = ['$scope', 'seClient', '$log', '$location'];
function LoginController(scope, seClient, $log, $location){
    var userStore;
    /* jshint ignore:start */
    userStore = new Oidc.WebStorageStateStore({ store: window.localStorage });
    /* jshint ignore:end */
    var langCode ='fr-FR'; //This should be dynamic Language selection from application
    var countryCode = 'FR'; //This should be dynamically selected country
   //var countryObj = loginService.getSelectedCountryData();
    //This line will be ignored
   scope.settings = {
        authority: seClient.IDMS_AUTODISCOVERY_URL,
        client_id: seClient.CLIENT_ID,
        redirect_uri:seClient.REDIRECT_URL,
        extraQueryParams: {app: seClient.APP_ID},
        response_type: 'id_token token',
        scope: 'openid+profile',
        silent_redirect_uri:seClient.SILENT_REDIRECT_URL,
        post_logout_redirect_uri:seClient.REDIRECT_URL,
        automaticSilentRenew:true,
        includeIdTokenInSilentRenew: true,
        revokeAccessTokenOnSignout: true,
        monitorSession: true,
        silentRequestTimeout:10000,
        filterProtocolClaims: false,
        loadUserInfo: false,
        userStore: userStore 
    };
    
    /* jshint ignore:start */
    scope.client = new Oidc.UserManager(scope.settings);
    /* jshint ignore:end */
    var extraParam= {
        "app":seClient.APP_ID,
        "lang":langCode,
        "countryCode":countryCode
    };
    window.localStorage.setItem("loginMode", "login");
    scope.client.signinRedirect({'extraQueryParams':extraParam})
        .then(function (user) {
        $log.log("signed in", user);
    })
    .catch(function (err) {
        $log.log(err);
        $location.path("/");
    });
}
