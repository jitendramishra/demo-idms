'use strict';

angular.module('se-branding')
    .controller('landingPage', ['$scope', '$window', 'ngOidcClient', 'seService','$rootScope', function($scope, $window, NgOidcClient, seService, $rootScope) {
       $scope.isLoggedIn = false;
       $rootScope.$on("ng-oidcclient-userinfo-changed-event", function(){     
        NgOidcClient.getUserObjFromSession().then(function(result){
            if(result) {
                $scope.$apply(function(){
                    $scope.isLoggedIn = true;
                }); 
                $scope.loginUser=result.profile;
            }
        });
    });
       $scope.login = function() {
           var languageCode = 'fr-FR';
           var countryCode='FR';
           //Set the Login Mode to retrieve later 
           seService.setLoginMode('login');
           NgOidcClient.signRedirect(countryCode, languageCode);
       }
       $scope.logout = function() {
           seService.removeToken();
           NgOidcClient.logOutRedirect();
       }
       $scope.register = function() {
           //fr-FR is the ISO Langeuage code and 'FR' is the Country code
           seService.register('fr-FR', 'FR');
       }
       $scope.updateProfile = function() {
           seService.updateProfile('fr-Fr', 'FR');
       }
       
       init();

       function init() {
        if($window.location.hash.indexOf('access_token')!==-1) {
            seService.setAccessTokenURL($window.location.hash);
        } else {
            seService.setAccessTokenURL(undefined);
        }
        //This is normal Login(Need to capture if user logged in)
        if(seService.getAccessTokenURL && seService.getAccessTokenURL().indexOf('access_token')!==-1 && seService.getLoginMode()==='login') {
            //Get the access Token URL
            var url = seService.getAccessTokenURL();
            var parsedURL = url.split('#');
            //Parse it the library expect it , normally Angualr put an extra '/' after '#', In order to work with library we need to put the extra '/'
            var formmedURL = parsedURL[0]+"#"+parsedURL[1].slice(1, parsedURL[1].length)+"&expires_in="+(45 * 60);
            //Once you formmed url call oidc to parse the result 
            NgOidcClient.signinRedirectCallback(formmedURL).then(function(result){
                if(result) {;
                    $scope.$apply(function(){
                        $scope.isLoggedIn = true;
                    });    
                    seService.setAccessTokenURL(undefined);
                    seService.setToken(result.access_token);
                    $scope.loginUser=result.profile;
                }
            });
        } else {
            var isEditProfile = seService.getIsEditProfile();
            if(isEditProfile) {
                seService.setIsEditProfile(false);
                NgOidcClient.getUserProfileByAccessToken();
            } else {
                NgOidcClient.getUserObjFromSession().then(function(result){
                    if(result) {
                        $scope.$apply(function(){
                            $scope.isLoggedIn = true;
                        }); 
                        $scope.loginUser=result.profile;
                    } else {
                        //You need to write Logic for Force Login here 
                    }
                });
            }
        }
       }
   }]);