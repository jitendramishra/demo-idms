'use strict';
/**
 * @ngdoc brandingModule
 * @name se-branding
 * @description
 * # se-branding
 *
 * Main module of the application.
 */
  angular
    .module('se-branding', [
      'seWebUI',
	    'gettext',
      'ui.bootstrap',
      'se-branding-config',
      'ng.oidcclient' //Here you can include the submodule of ngOidc Client(So this is designed becuase you can plug and play the module anywhere you want it)
    ])
    .config(['$routeProvider', 'ngOidcClientProvider', 'seClient',function($routeProvider, ngOidcClientProvider, seClient) {
      var settings, userStore;
      //Below line is jshint ignore becuase Oidc is a global variable where as jshint not finding the Oidc as global
      /* jshint ignore:start */
      userStore = new Oidc.WebStorageStateStore({ store: window.localStorage });
      /* jshint ignore:end */
        settings = {
            authority: seClient.IDMS_AUTODISCOVERY_URL, // this is the autoDiscovery url of idms
            client_id: seClient.CLIENT_ID, //Here you go the client id which is registered with idms
            redirect_uri:seClient.REDIRECT_URL, //Here after Login define the redirect url
            extraQueryParams: {app: seClient.APP_ID}, //Every connected app will have different appid
            response_type: 'id_token token', //This will give id_token and token
            scope: 'openid+profile',
            silent_redirect_uri:seClient.SILENT_REDIRECT_URL, //This is the configuration for redirect callback for Token Renewel
            post_logout_redirect_uri:seClient.REDIRECT_URL, //This is configuration where post logout redirect callback is defined
            automaticSilentRenew:true, //Silent Renew of token will happen if it is true
            includeIdTokenInSilentRenew: true,
            revokeAccessTokenOnSignout: false, 
            monitorSession: true,
            silentRequestTimeout:10000, //After certain time if the token is not renwed , here it define the timeout
            filterProtocolClaims: false,
            loadUserInfo: false,
            userStore: userStore             
        };
        ngOidcClientProvider.setSetting(settings);

     $routeProvider
        .when('/homepage', {
          templateUrl: 'views/se-landing.html',
          resolve: {
            app: ['$rootScope', 'seLandingMetaInfo', function($rootScope, seLandingMetaInfo) { 
              seLandingMetaInfo.getMetaInfo().then(function(data) {
                $rootScope.metatags = data.homepage;
              });
              //return defer.promise;
            }]
          }
        })
        .otherwise({
          redirectTo: '/homepage'

        });
    
        
    }]) .constant('loginConstants',(function() {
      return {
        SESSION_MANAGER_LOGOUT_URL : '/logout',
        USER_SERVICE_URL: '/api/v1/user',
        SESSION_MANAGER_API_TKN: '/v2/token',
        SESSION_MANAGER_LOGIN : '/login',
        SESSION_MANAGER_RENEW_TOKEN : '/v2/refreshToken'
    }
    })());
    